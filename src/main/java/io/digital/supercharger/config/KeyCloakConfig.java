package io.digital.supercharger.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class KeyCloakConfig {
  @Value("${keycloak.credentials.secret:d601ec7a-e5f5-4cca-b434-3b20103585b3}")
  private String clientSecret;

  @Value("${keycloak.clientId:client-confidential-app}")
  private String clientId;

  @Value("${keycloak.auth-server-url:http://localhost:8180}")
  private String serverUrl;

  @Value("${keycloak.realm:dev}")
  private String realm;

  @Value("${keycloak.admin.user:admin}")
  private String adminUser;

  @Value("${keycloak.admin.password:admin}")
  private String adminPassword;

  @Value("${keycloak.admin.realm:master}")
  private String adminRealm;

  @Value("${keycloak.admin.clientId:admin-cli}")
  private String adminClientId;
}
