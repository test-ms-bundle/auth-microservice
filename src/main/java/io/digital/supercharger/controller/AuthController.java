package io.digital.supercharger.controller;

import static io.digital.supercharger.common.util.RestUtil.getAuthorizationTokenWithoutBearer;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.common.util.RestUtil;
import io.digital.supercharger.dto.AuthenticationResponse;
import io.digital.supercharger.dto.ClientInfo;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.UserPassword;
import io.digital.supercharger.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@Api(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@Log4j2
public class AuthController {

  private final AuthService authService;

  public AuthController(AuthService authService) {
    this.authService = authService;
  }

  /**
   * Login To IDM.
   *
   * @param userCredentials The Username & Password.
   * @param request The Request to retrieve the IP Address.
   * @return The user token & refresh token.
   * @see AuthenticationResponse
   */
  @PostMapping("login")
  @ApiOperation(value = "Login")
  public AuthenticationResponse login(
      @RequestBody @Valid UserCredentials userCredentials, final HttpServletRequest request) {
    String clientIpAddress = RestUtil.getClientIp(request);
    log.info(
        "Login Request from {} with IP Address {}", userCredentials.getUsername(), clientIpAddress);
    return authService.login(userCredentials, clientIpAddress);
  }

  /**
   * Logout with Refresh token.
   *
   * @param token The Logged in Token
   * @param refreshToken The Refresh token
   * @return Response Entity with status 204
   * @see ResponseEntity
   */
  @PostMapping("logout")
  @ApiOperation(value = "Logout Token (Refresh Token is Required)")
  public ResponseEntity<String> logout(
      @RequestHeader("Authorization") String token,
      @Required @RequestHeader("refresh-token") String refreshToken) {
    log.info("User Requesting Logout");
    return authService.logout(getAuthorizationTokenWithoutBearer(token), refreshToken);
  }

  /**
   * Get User Info from Backend.
   *
   * @param token The user token
   * @param offline If set to true, the token will not be validated with backend
   * @return The Logged in User Info
   * @see ClientInfo
   */
  @PostMapping("userinfo")
  @ApiOperation(value = "Get User Info")
  public ClientInfo getUserInfo(
      @Required @RequestHeader("Authorization") String token,
      @RequestHeader(name = "offline", defaultValue = "true", required = false)
          final boolean offline) {
    log.info("Check the token information");
    return authService.getUserInfo(getAuthorizationTokenWithoutBearer(token), offline);
  }

  /**
   * Get User Info from Backend.
   *
   * @param token The user token
   * @param password Current & New Password
   * @param request The Request to retrieve the IP Address
   * @return The Logged in User Info
   * @see ClientInfo
   */
  @PutMapping("password")
  @ApiOperation(value = "Update User Password")
  public ResponseEntity<String> updatePassword(
      @Required @RequestHeader("Authorization") String token,
      @RequestBody @Valid UserPassword password,
      final HttpServletRequest request)
      throws JsonProcessingException {
    log.info("User Request Update Password");
    String clientIpAddress = RestUtil.getClientIp(request);
    return authService.updatePassword(
        getAuthorizationTokenWithoutBearer(token), password, clientIpAddress);
  }

  /**
   * Refresh generated token.
   *
   * @param token The Current Token.
   * @param refreshToken Refresh Token.
   * @return The Authentication Response
   * @see AuthenticationResponse
   */
  @PostMapping("token/refresh")
  @ApiOperation(value = "Refresh the Token (Refresh Token is Required)")
  public AuthenticationResponse refresh(
      @RequestHeader("Authorization") String token,
      @Required @RequestHeader("refresh-token") String refreshToken,
      HttpServletRequest request) {
    log.info("Refresh user token");
    String clientIpAddress = RestUtil.getClientIp(request);
    log.info("Refresh user token with IP Address {}", clientIpAddress);
    return authService.refreshToken(
        getAuthorizationTokenWithoutBearer(token), refreshToken, clientIpAddress);
  }

  /**
   * Check Token Validity with backend services.
   *
   * @param token Valid Token
   * @return The User Info Response
   * @see ClientInfo
   */
  @PostMapping("token/introspect")
  @ApiOperation(value = "Get User Info, and Validates if user still active in backend")
  public ClientInfo getTokenIntrospect(@Required @RequestHeader("Authorization") String token) {
    log.info("Checking if Token is Authorized");
    return authService.getTokenIntrospect(getAuthorizationTokenWithoutBearer(token));
  }

  /**
   * Strictly Internal API ##THIS IS JUST AN EXAMPLE###.
   *
   * @param createClientInfo The client to be created
   * @param request To retrieve the IP Address
   * @return The created Client Info with
   * @see ClientInfo
   */
  @PostMapping("users")
  @ApiOperation(value = "Create a new client (This is a Demo, security process needs to be added)")
  public ResponseEntity<String> create(
      @RequestBody @Valid CreateClientInfo createClientInfo, final HttpServletRequest request)
      throws JsonProcessingException {
    String clientIpAddress = RestUtil.getClientIp(request);
    log.info(
        "Login Request from {} with IP Address {}",
        createClientInfo.getUsername(),
        clientIpAddress);
    return authService.create(createClientInfo, clientIpAddress);
  }
}
