package io.digital.supercharger.dto;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class UserPassword {
  @NotEmpty private String currentPassword;
  @NotEmpty private String newPassword;
}
