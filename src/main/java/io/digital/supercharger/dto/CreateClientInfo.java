package io.digital.supercharger.dto;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateClientInfo {
  private long cif;
  private boolean emailVerified;
  @NotEmpty private String name;
  private String phoneNumber;
  @NotEmpty private String username;
  @NotEmpty private String firstName;
  @NotEmpty private String lastName;
  private String picture;
  @NotEmpty private String email;
  @NotEmpty private List<String> groups;
  // More Logic should come here for password validation.
  @NotEmpty private String password;
}
