package io.digital.supercharger.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserCredentials {
  @ApiModelProperty(example = "demo")
  @NotEmpty
  private String username;

  @ApiModelProperty(example = "password")
  @NotEmpty
  private String password;
}
