package io.digital.supercharger.dto.keycloak;

import com.auth0.jwt.interfaces.Claim;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Log4j2
public class KeyCloakUserResponse {
  private boolean active;
  private long cif;
  private String sub;

  @JsonProperty("email_verified") // Name is different in KeyCloak
  private boolean emailVerified;

  private String name;

  @JsonProperty("phone_number") // Name is different in KeyCloak
  private String phoneNumber;

  @JsonProperty("preferred_username") // Name is different in KeyCloak
  private String preferredUsername;

  @JsonProperty("given_name")
  private String givenName;

  @JsonProperty("family_name")
  private String familyName;

  private String picture;
  private String email;

  /**
   * Convert Claim Map to KeyCloak User Response.
   *
   * @param claimMap The Claim map from the JWT Token
   */
  @JsonIgnore
  public KeyCloakUserResponse(Map<String, Claim> claimMap) {
    claimMap.forEach(
        (key, claim) -> {
          switch (key) {
            case "cif":
              cif = claim.asLong();
              break;
            case "sub":
              sub = claim.asString();
              break;
            case "email_verified":
              emailVerified = claim.asBoolean();
              break;
            case "name":
              name = claim.asString();
              break;
            case "phone_number":
              phoneNumber = claim.asString();
              break;
            case "preferred_username":
              preferredUsername = claim.asString();
              break;
            case "given_name":
              givenName = claim.asString();
              break;
            case "family_name":
              familyName = claim.asString();
              break;
            case "picture":
              picture = claim.asString();
              break;
            case "email":
              email = claim.asString();
              break;
            default:
              break;
          }
        });
  }
}
