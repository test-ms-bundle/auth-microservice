package io.digital.supercharger.dto.keycloak;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class KeyCloakAuthResponse {
  @JsonProperty("access_token") // Name is different in KeyCloak
  private String accessToken;

  @JsonProperty("expires_in") // Name is different in KeyCloak
  private int expiresIn;

  @JsonProperty("refresh_expires_in") // Name is different in KeyCloak
  private int refreshExpiresIn;

  @JsonProperty("refresh_token") // Name is different in KeyCloak
  private String refreshToken;

  @JsonProperty("token_type") // Name is different in KeyCloak
  private String tokenType;
}
