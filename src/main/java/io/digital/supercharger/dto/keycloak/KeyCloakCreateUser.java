package io.digital.supercharger.dto.keycloak;

import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KeyCloakCreateUser {
  private String username;
  private boolean enabled;
  private boolean emailVerified;
  private String firstName;
  private String lastName;
  private String email;
  List<KeyCloakPassword> credentials;
  List<String> groups;
  Map<String, List<String>> attributes;
}
