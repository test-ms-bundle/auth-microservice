package io.digital.supercharger.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class ClientInfo {
  private String id;
  private long cif;
  private boolean emailVerified;
  private String name;
  private String phoneNumber;
  private String username;
  private String firstName;
  private String lastName;
  private String picture;
  private String email;
}
