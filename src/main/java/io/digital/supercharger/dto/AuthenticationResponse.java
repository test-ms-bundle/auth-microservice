package io.digital.supercharger.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class AuthenticationResponse {
  private String accessToken;
  private int expiresIn;
  private int refreshExpiresIn;
  private String refreshToken;
  private String tokenType;
}
