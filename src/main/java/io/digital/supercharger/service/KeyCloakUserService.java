package io.digital.supercharger.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.common.util.JsonUtil;
import io.digital.supercharger.config.KeyCloakConfig;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.keycloak.KeyCloakAuthResponse;
import io.digital.supercharger.dto.keycloak.KeyCloakCreateUser;
import io.digital.supercharger.dto.keycloak.KeyCloakPassword;
import io.digital.supercharger.dto.keycloak.KeyCloakUserResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Component
@Log4j2
public class KeyCloakUserService {
  private static final String GRANT_TYPE = "grant_type";
  private static final String REFRESH_TOKEN = "refresh_token";
  private static final String CLIENT_ID = "client_id";
  private static final String CLIENT_SECRET = "client_secret";
  private static final String X_FORWARDED_FOR = "X-Forwarded-For";
  private static final String PASS_KEY = "password";

  private final KeyCloakConfig keyCloakConfig;

  @Qualifier("genericRestTemplate")
  private final RestTemplate restTemplate;

  /**
   * Initialise KeyCloakUserService with KeyCloak Config.
   *
   * @param keyCloakConfig The Configuration of KeyCloak Server.
   * @param restTemplate The Generic Rest template from common.
   */
  public KeyCloakUserService(KeyCloakConfig keyCloakConfig, RestTemplate restTemplate) {
    this.keyCloakConfig = keyCloakConfig;
    this.restTemplate = restTemplate;
    restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(keyCloakConfig.getServerUrl()));
  }

  /**
   * Login user with UserCredentials & IpAddress.
   *
   * @param userCredentials The user credentials.
   * @param ipAddress This is important to keep track per user, KeyCloak must be running
   *     PROXY_ADDRESS_FORWARDING=true mode.
   * @return The KeyCloak Auth Response
   * @see KeyCloakAuthResponse
   */
  public KeyCloakAuthResponse login(final UserCredentials userCredentials, String ipAddress) {
    return login(userCredentials.getUsername(), userCredentials.getPassword(), ipAddress, false);
  }

  private KeyCloakAuthResponse login(
      String username, String password, String ipAddress, boolean isAdmin) {
    var headers = getHttpHeaders("");
    headers.add(X_FORWARDED_FOR, ipAddress);
    MultiValueMap<String, String> map = getUserMap(isAdmin);
    map.add("username", username);
    map.add(PASS_KEY, password);
    map.add(GRANT_TYPE, PASS_KEY);

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    String realm = getRealm(isAdmin);

    ResponseEntity<KeyCloakAuthResponse> response =
        restTemplate.postForEntity(
            getOpenIdConnectUrl(realm, "/token"), request, KeyCloakAuthResponse.class);

    return response.getBody();
  }

  private String getRealm(boolean isAdmin) {
    var realm = keyCloakConfig.getRealm();
    if (isAdmin) {
      realm = keyCloakConfig.getAdminRealm();
    }
    return realm;
  }

  /**
   * User info retrieves the user information, but wont check if the user is still active.
   *
   * @param token the user token
   * @return The Authenticated Token user Info
   * @see KeyCloakUserResponse
   */
  public KeyCloakUserResponse getUserInfo(String token) {
    var headers = getHttpHeaders(token);
    var request = new HttpEntity<>(getUserMap(), headers);

    ResponseEntity<KeyCloakUserResponse> response =
        restTemplate.postForEntity(
            getOpenIdConnectUrl("/userinfo"), request, KeyCloakUserResponse.class);
    return response.getBody();
  }

  /**
   * Retrieves the user information and if the user is still active in the backend. Also checks the
   * token validity.
   *
   * @param token the user token
   * @return The authenticated token User Info
   * @see KeyCloakUserResponse
   */
  public KeyCloakUserResponse getTokenIntrospect(String token) {
    var map = getUserMap();
    map.add("token", token);

    HttpEntity<MultiValueMap<String, String>> request =
        new HttpEntity<>(map, getHttpHeaders(token));

    ResponseEntity<KeyCloakUserResponse> response =
        restTemplate.postForEntity(
            getOpenIdConnectUrl("/token/introspect"), request, KeyCloakUserResponse.class);
    return response.getBody();
  }

  /**
   * Logout refresh token from KeyCloak.
   *
   * @param token The user token
   * @param refreshToken The Refresh token
   * @return ResponseEntity with status 204
   * @see ResponseEntity
   */
  public ResponseEntity<String> logout(String token, String refreshToken) {
    return logout(token, refreshToken, false);
  }

  private ResponseEntity<String> logout(String token, String refreshToken, boolean isAdmin) {
    var map = getUserMap(isAdmin);
    map.add(REFRESH_TOKEN, refreshToken);
    HttpEntity<MultiValueMap<String, String>> request =
        new HttpEntity<>(map, getHttpHeaders(token));

    String realm = getRealm(isAdmin);
    var loggedOut =
        restTemplate.postForEntity(
            getOpenIdConnectUrl(realm, "/logout"), request, ResponseEntity.class);

    return new ResponseEntity<>(loggedOut.getStatusCode());
  }

  /**
   * Refresh the token in KeyCloak.
   *
   * @param token the user token
   * @param refreshToken The Refresh token
   * @param ipAddress The User Ip Address
   * @return The new token and refresh token
   * @see KeyCloakAuthResponse
   */
  public KeyCloakAuthResponse refreshToken(String token, String refreshToken, String ipAddress) {
    var map = getUserMap();
    map.add(REFRESH_TOKEN, refreshToken);
    map.add(GRANT_TYPE, REFRESH_TOKEN);

    var headers = getHttpHeaders(token);
    headers.add(X_FORWARDED_FOR, ipAddress);
    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    ResponseEntity<KeyCloakAuthResponse> response =
        restTemplate.postForEntity(
            getOpenIdConnectUrl("/token"), request, KeyCloakAuthResponse.class);

    return response.getBody();
  }

  /**
   * Get the Open ID Certificate, this is used to validate the JWT Token offline.
   *
   * @return KeyCloak Open ID certificate url
   */
  public String getOpenIdCert() {
    return keyCloakConfig.getServerUrl() + getOpenIdConnectUrl("/certs");
  }

  /**
   * Updates the User Password with Admin User.
   *
   * @param userId The User Id that needs to be updated
   * @param password The New Password
   * @param clientIpAddress The User Ip Address
   * @return 204 No Content if Updated Or Bad Request in case of error or failure.
   */
  public ResponseEntity<String> updatePassword(
      String userId, String password, String clientIpAddress) throws JsonProcessingException {

    var resetPasswordJson =
        JsonUtil.toJson(
            KeyCloakPassword.builder().temporary(false).type(PASS_KEY).value(password).build());

    var adminLogin = loginAdmin(clientIpAddress);
    var request = getAdminRequest(adminLogin.getAccessToken(), clientIpAddress, resetPasswordJson);
    var updateUrl =
        keyCloakConfig.getServerUrl()
            + "/auth/admin/realms/"
            + keyCloakConfig.getRealm()
            + "/users/"
            + userId
            + "/reset-password";

    var update = restTemplate.exchange(updateUrl, HttpMethod.PUT, request, ResponseEntity.class);
    log.info("Admin Logout request");
    logout(adminLogin.getAccessToken(), adminLogin.getRefreshToken(), true);
    return new ResponseEntity<>(update.getStatusCode());
  }

  /**
   * Create a user in KeyCloak. This section needs refactoring with better security checks.
   *
   * @param createClientInfo The create Client Object
   * @param clientIpAddress The user IP Address
   * @return 201 Created
   */
  public ResponseEntity<String> createUser(
      final CreateClientInfo createClientInfo, String clientIpAddress)
      throws JsonProcessingException {

    Map<String, List<String>> attributes = new HashMap<>();
    attributes.put("cif", List.of(String.valueOf(createClientInfo.getCif())));
    attributes.put("phoneNumber", List.of(createClientInfo.getPhoneNumber()));
    attributes.put("picture", List.of(createClientInfo.getPicture()));

    var transformClientInfoToKeyCloak =
        JsonUtil.toJson(
            KeyCloakCreateUser.builder()
                .username(createClientInfo.getUsername())
                .enabled(true)
                .groups(createClientInfo.getGroups())
                .lastName(createClientInfo.getLastName())
                .firstName(createClientInfo.getFirstName())
                .emailVerified(createClientInfo.isEmailVerified())
                .email(createClientInfo.getEmail())
                .attributes(attributes)
                .credentials(
                    List.of(
                        KeyCloakPassword.builder()
                            .temporary(false)
                            .type(PASS_KEY)
                            .value(createClientInfo.getPassword())
                            .build()))
                .build());

    var adminLogin = loginAdmin(clientIpAddress);
    var request =
        getAdminRequest(
            adminLogin.getAccessToken(), clientIpAddress, transformClientInfoToKeyCloak);

    var createUserUrl =
        keyCloakConfig.getServerUrl()
            + "/auth/admin/realms/"
            + keyCloakConfig.getRealm()
            + "/users";

    var update =
        restTemplate.exchange(createUserUrl, HttpMethod.POST, request, ResponseEntity.class);
    log.info("Admin Logout request");
    logout(adminLogin.getAccessToken(), adminLogin.getRefreshToken(), true);
    return new ResponseEntity<>(update.getStatusCode());
  }

  private KeyCloakAuthResponse loginAdmin(String ipAddress) {
    var adminUser = keyCloakConfig.getAdminUser();
    var adminPassword = keyCloakConfig.getAdminPassword();
    return login(adminUser, adminPassword, ipAddress, true);
  }

  private HttpEntity<String> getAdminRequest(String adminToken, String ipAddress, String data) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setBearerAuth(adminToken);
    headers.add(X_FORWARDED_FOR, ipAddress);
    return new HttpEntity<>(data, headers);
  }

  private HttpHeaders getHttpHeaders(String token) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    if (!token.isEmpty()) {
      headers.setBearerAuth(token);
    }
    return headers;
  }

  private String getOpenIdConnectUrl(String path) {
    return getOpenIdConnectUrl(keyCloakConfig.getRealm(), path);
  }

  private String getOpenIdConnectUrl(String realm, String path) {
    return "/auth/realms/" + realm + "/protocol/openid-connect" + path;
  }

  private MultiValueMap<String, String> getUserMap() {
    return getUserMap(false);
  }

  private MultiValueMap<String, String> getUserMap(boolean isAdmin) {
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    var clientId = keyCloakConfig.getClientId();
    if (isAdmin) {
      clientId = keyCloakConfig.getAdminClientId();
    } else {
      map.add(CLIENT_SECRET, keyCloakConfig.getClientSecret());
    }
    map.add(CLIENT_ID, clientId);
    return map;
  }
}
