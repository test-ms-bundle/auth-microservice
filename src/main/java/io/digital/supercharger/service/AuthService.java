package io.digital.supercharger.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.dto.AuthenticationResponse;
import io.digital.supercharger.dto.ClientInfo;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.UserPassword;
import org.springframework.http.ResponseEntity;

public interface AuthService {

  /**
   * Login to backend service with Username & password.
   *
   * @param userCredentials The Username & Password.
   * @param ipAddress The Client Ip Address.
   * @return The Authentication Response
   * @see AuthenticationResponse
   */
  AuthenticationResponse login(final UserCredentials userCredentials, String ipAddress);

  /**
   * Get Logged In User Info, This endpoint does not check if the user is still active.
   *
   * @param token The Authenticated Token.
   * @param offline Check the JWT Token identity without going to backend
   * @return The User Token
   * @see ClientInfo
   */
  ClientInfo getUserInfo(String token, boolean offline);

  /**
   * Get Logged In User Info, This endpoint validates the user still active. Returns also the roles
   * of the user.
   *
   * @param token The Authenticated Token.
   * @return The Token Introspect, if the token is invalid it will Throw UAuthorised
   * @see ClientInfo
   */
  ClientInfo getTokenIntrospect(String token);

  /**
   * Logout a user and remove refresh token.
   *
   * @param token The Logged in Token
   * @param refreshToken The Refresh Token
   * @return Http status of 204
   * @see ResponseEntity
   */
  ResponseEntity<String> logout(String token, String refreshToken);

  /**
   * Refresh the user token.
   *
   * @param token The Logged in Token
   * @param refreshToken The Refresh Token
   * @param ipAddress The Client Ip Address
   * @return Return the Authentication Response
   * @see AuthenticationResponse
   */
  AuthenticationResponse refreshToken(String token, String refreshToken, String ipAddress);

  /**
   * Checks the current user password.
   *
   * @param token The Current Token
   * @param password The Password Object
   * @param ipAddress The Client Ip Address
   * @return 204 no content when password is updated
   */
  ResponseEntity<String> updatePassword(String token, final UserPassword password, String ipAddress)
      throws JsonProcessingException;

  /**
   * Create a new user in KeyCloak.
   *
   * @param createClientInfo create client info
   * @param clientIpAddress The IP address
   * @return 201 Created
   */
  ResponseEntity<String> create(final CreateClientInfo createClientInfo, String clientIpAddress)
      throws JsonProcessingException;
}
