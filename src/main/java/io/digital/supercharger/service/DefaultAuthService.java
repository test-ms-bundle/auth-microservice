package io.digital.supercharger.service;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.dto.AuthenticationResponse;
import io.digital.supercharger.dto.ClientInfo;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.UserPassword;
import io.digital.supercharger.dto.keycloak.KeyCloakAuthResponse;
import io.digital.supercharger.dto.keycloak.KeyCloakUserResponse;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
@Log4j2
public class DefaultAuthService implements AuthService {

  private KeyCloakUserService keyCloakUserService;

  public DefaultAuthService(KeyCloakUserService keyCloakUserService) {
    this.keyCloakUserService = keyCloakUserService;
  }

  @Override
  public AuthenticationResponse login(final UserCredentials userCredentials, String ipAddress) {
    var loggedInUser = keyCloakUserService.login(userCredentials, ipAddress);
    return getAuthenticationResponse(loggedInUser);
  }

  @Override
  public AuthenticationResponse refreshToken(String token, String refreshToken, String ipAddress) {
    return getAuthenticationResponse(
        keyCloakUserService.refreshToken(token, refreshToken, ipAddress));
  }

  @Override
  public ResponseEntity<String> updatePassword(
      String token, UserPassword password, String ipAddress) throws JsonProcessingException {
    // Check if User is Active
    var user = getTokenIntrospect(token);
    var userId = user.getId();
    log.info("User Introspect was correct {} user is Active.", userId);
    // Login the user to make sure the password is correct
    var userCredentials =
        UserCredentials.builder()
            .username(user.getUsername())
            .password(password.getCurrentPassword())
            .build();
    var loginUserWithPassword = login(userCredentials, ipAddress);
    log.info(
        "User {} Logged in for {}. Proceeding with Update Password",
        userId,
        loginUserWithPassword.getExpiresIn());
    return keyCloakUserService.updatePassword(userId, password.getNewPassword(), ipAddress);
  }

  @Override
  public ResponseEntity<String> create(
      final CreateClientInfo createClientInfo, String clientIpAddress)
      throws JsonProcessingException {
    log.info(
        "Create new user {} client IpAddress in for {}. Proceeding with Update Password",
        createClientInfo.getUsername(),
        clientIpAddress);
    return keyCloakUserService.createUser(createClientInfo, clientIpAddress);
  }

  @Override
  public ClientInfo getUserInfo(String token, boolean offline) {
    if (offline) {
      log.info("Token request offline.");
      return verifyAndGetUserInfoFromToken(token);
    } else {
      return getUserInfResponse(keyCloakUserService.getUserInfo(token));
    }
  }

  @Override
  public ClientInfo getTokenIntrospect(String token) {
    var keyCloakAuthResponse = keyCloakUserService.getTokenIntrospect(token);
    if (!keyCloakAuthResponse.isActive()) {
      throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
    }
    return getUserInfResponse(keyCloakAuthResponse);
  }

  @Override
  public ResponseEntity<String> logout(String token, String refreshToken) {
    return keyCloakUserService.logout(token, refreshToken);
  }

  private ClientInfo verifyAndGetUserInfoFromToken(String token) {
    var decodedJwt = verifyAndGetDecodedJwtToken(token);
    return getUserInfResponse(new KeyCloakUserResponse(decodedJwt.getClaims()));
  }

  private DecodedJWT verifyAndGetDecodedJwtToken(String token) {
    try {
      var decodedJwt = JWT.decode(token);
      JwkProvider provider =
          new JwkProviderBuilder(new URL(keyCloakUserService.getOpenIdCert())).build();
      Jwk jwk = provider.get(decodedJwt.getKeyId());
      Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);

      Verification verifier = JWT.require(algorithm);
      verifier.build().verify(decodedJwt);
      return decodedJwt;
    } catch (JWTVerificationException | JwkException e) {
      log.warn("Verification Exception: " + e.getMessage());
    } catch (MalformedURLException e) {
      log.error("MalformedURLException to verify token: " + e.getMessage());
    }
    throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
  }

  private AuthenticationResponse getAuthenticationResponse(
      final KeyCloakAuthResponse keyCloakAuthResponse) {
    return AuthenticationResponse.builder()
        .accessToken(keyCloakAuthResponse.getAccessToken())
        .expiresIn(keyCloakAuthResponse.getExpiresIn())
        .refreshExpiresIn(keyCloakAuthResponse.getRefreshExpiresIn())
        .refreshToken(keyCloakAuthResponse.getRefreshToken())
        .tokenType(keyCloakAuthResponse.getTokenType())
        .build();
  }

  private ClientInfo getUserInfResponse(final KeyCloakUserResponse keyCloakAuthResponse) {
    return ClientInfo.builder()
        .cif(keyCloakAuthResponse.getCif())
        .email(keyCloakAuthResponse.getEmail())
        .emailVerified(keyCloakAuthResponse.isEmailVerified())
        .firstName(keyCloakAuthResponse.getGivenName())
        .lastName(keyCloakAuthResponse.getFamilyName())
        .username(keyCloakAuthResponse.getPreferredUsername())
        .phoneNumber(keyCloakAuthResponse.getPhoneNumber())
        .picture(keyCloakAuthResponse.getPicture())
        .name(keyCloakAuthResponse.getName())
        .id(keyCloakAuthResponse.getSub())
        .build();
  }
}
