package io.digital.supercharger.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor;
import io.digital.supercharger.common.util.JsonUtil;
import io.digital.supercharger.dto.AuthenticationResponse;
import io.digital.supercharger.dto.ClientInfo;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.UserPassword;
import io.digital.supercharger.exception.RestExceptionHandler;
import io.digital.supercharger.service.AuthService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
class AuthControllerTest {

  @Mock AuthenticationInterceptor interceptor;
  private MockMvc mockMvc;
  @Mock private AuthService authService;

  @BeforeEach
  void setUp() throws Exception {
    when(interceptor.preHandle(
            any(HttpServletRequest.class), any(HttpServletResponse.class), any()))
        .thenReturn(true);

    this.mockMvc =
        standaloneSetup(new AuthController(authService))
            .setControllerAdvice(new RestExceptionHandler())
            .addInterceptors(interceptor)
            .build();
  }

  @Test
  void login() throws Exception {
    var authenticationResponse =
        AuthenticationResponse.builder()
            .accessToken("test_token")
            .expiresIn(300)
            .tokenType("bearer")
            .refreshExpiresIn(1500)
            .refreshToken("some_token")
            .build();
    UserCredentials userCredentials = new UserCredentials("username", "password");

    when(authService.login(
            argThat(
                login ->
                    login.getUsername().equals("username")
                        && login.getPassword().equals("password")),
            any(String.class)))
        .thenReturn(authenticationResponse);
    this.mockMvc
        .perform(
            post("/api/auth/login")
                .content(JsonUtil.toJson(userCredentials))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.expiresIn").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.refreshExpiresIn").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.refreshToken").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.tokenType").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.accessToken").exists());
    verify(authService, times(1)).login(any(UserCredentials.class), any(String.class));
  }

  @Test
  void logoutWithoutToken() throws Exception {
    this.mockMvc
        .perform(
            post("/api/auth/logout")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    verify(authService, times(0)).logout(any(String.class), any(String.class));
  }

  @Test
  void logout() throws Exception {
    final String authValue = "x1";
    final String refreshTokenValue = "x2";
    var responseEntity = new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    when(authService.logout(
            argThat(token -> token.equals(authValue)),
            argThat(token -> token.equals(refreshTokenValue))))
        .thenReturn(responseEntity);
    var headers = new HttpHeaders();
    headers.add("Authorization", authValue);
    headers.add("refresh-token", refreshTokenValue);
    this.mockMvc
        .perform(
            post("/api/auth/logout")
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    verify(authService, times(1)).logout(eq(authValue), eq(refreshTokenValue));
  }

  @Test
  void userInfoWithoutToken() throws Exception {
    this.mockMvc
        .perform(
            post("/api/auth/userinfo")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    verify(authService, times(0)).getUserInfo(any(String.class), eq(true));
  }

  @Test
  void userInfo() throws Exception {
    final String authValue = "x1";
    var userInfoResponse =
        ClientInfo.builder()
            .cif(123456789)
            .picture("https://someimage.com/test")
            .phoneNumber("+97123456778")
            .email("test@test.com")
            .username("test")
            .build();

    when(authService.getUserInfo(argThat(authHeader -> authHeader.equals(authValue)), eq(false)))
        .thenReturn(userInfoResponse);
    var headers = new HttpHeaders();
    headers.add("Authorization", "Bearer " + authValue);
    headers.add("Offline", "false");
    this.mockMvc
        .perform(
            post("/api/auth/userinfo")
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cif").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").exists());
    verify(authService, times(1)).getUserInfo(eq(authValue), eq(false));
  }

  @Test
  void tokenIntrospectWithoutToken() throws Exception {
    this.mockMvc
        .perform(
            post("/api/auth/token/introspect")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    verify(authService, times(0)).getTokenIntrospect(any(String.class));
  }

  @Test
  void tokenIntrospect() throws Exception {
    final String authValue = "x1";
    var userInfoResponse =
        ClientInfo.builder()
            .cif(123456789)
            .picture("https://someimage.com/test")
            .phoneNumber("+97123456778")
            .email("test@test.com")
            .username("test")
            .build();

    when(authService.getTokenIntrospect(argThat(authHeader -> authHeader.equals(authValue))))
        .thenReturn(userInfoResponse);
    var headers = new HttpHeaders();
    headers.add("Authorization", "Bearer " + authValue);
    this.mockMvc
        .perform(
            post("/api/auth/token/introspect")
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cif").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").exists());
    verify(authService, times(1)).getTokenIntrospect(eq(authValue));
  }

  @Test
  void tokenRefreshWithoutRefreshToken() throws Exception {
    this.mockMvc
        .perform(
            post("/api/auth/token/refresh")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    verify(authService, times(0))
        .refreshToken(any(String.class), any(String.class), any(String.class));
  }

  @Test
  void tokenRefresh() throws Exception {
    final String authValue = "x1";
    final String tokenRefresh = "x2";
    var authenticationResponse =
        AuthenticationResponse.builder()
            .accessToken("test_token")
            .expiresIn(300)
            .tokenType("bearer")
            .refreshExpiresIn(1500)
            .refreshToken("some_token")
            .build();

    when(authService.refreshToken(
            argThat(authHeader -> authHeader.equals(authValue)),
            argThat(authHeader -> authHeader.equals(tokenRefresh)),
            any(String.class)))
        .thenReturn(authenticationResponse);
    var headers = new HttpHeaders();
    headers.add("Authorization", "Bearer " + authValue);
    headers.add("refresh-token", tokenRefresh);
    this.mockMvc
        .perform(
            post("/api/auth/token/refresh")
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.expiresIn").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.refreshExpiresIn").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.refreshToken").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.tokenType").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.accessToken").exists());
    verify(authService, times(1)).refreshToken(eq(authValue), eq(tokenRefresh), any(String.class));
  }

  @Test
  void updatePassword() throws Exception {
    final String authValue = "authValue";
    final String currentPassword = "pass1";
    final String newPassword = "pass2";
    UserPassword updatePassword = new UserPassword("pass1", "pass2");
    var responseEntity = new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    when(authService.updatePassword(
            argThat(authHeader -> authHeader.equals(authValue)),
            argThat(
                login ->
                    login.getCurrentPassword().equals(currentPassword)
                        && login.getNewPassword().equals(newPassword)),
            any(String.class)))
        .thenReturn(responseEntity);

    var headers = new HttpHeaders();
    headers.add("Authorization", authValue);

    this.mockMvc
        .perform(
            put("/api/auth/password")
                .content(JsonUtil.toJson(updatePassword))
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    verify(authService, times(1))
        .updatePassword(
            argThat(authHeader -> authHeader.equals(authValue)),
            argThat(
                login ->
                    login.getCurrentPassword().equals(currentPassword)
                        && login.getNewPassword().equals(newPassword)),
            any(String.class));
  }

  @Test
  void createUser() throws Exception {
    CreateClientInfo createClientInfo =
        CreateClientInfo.builder()
            .username("test")
            .cif(123456)
            .email("test@test.com")
            .emailVerified(true)
            .firstName("test")
            .lastName("test 1")
            .name("some name")
            .password("Test@@#")
            .groups(List.of("Clients"))
            .phoneNumber("123456")
            .picture("Some Picture Link")
            .build();

    var responseEntity = new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    when(authService.create(
            argThat(
                createClientInfoArg ->
                    createClientInfoArg.getEmail().equals(createClientInfo.getEmail())
                        && createClientInfoArg
                            .getPassword()
                            .equals(createClientInfo.getPassword())),
            any(String.class)))
        .thenReturn(responseEntity);

    this.mockMvc
        .perform(
            post("/api/auth/users")
                .content(JsonUtil.toJson(createClientInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    verify(authService, times(1))
        .create(
            argThat(
                createClientInfoArg ->
                    createClientInfoArg.getEmail().equals(createClientInfo.getEmail())
                        && createClientInfoArg
                            .getPassword()
                            .equals(createClientInfo.getPassword())),
            any(String.class));
  }
}
