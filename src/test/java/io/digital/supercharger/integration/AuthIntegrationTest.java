package io.digital.supercharger.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import io.digital.supercharger.TestHelper;
import io.digital.supercharger.common.exception.ApiError;
import io.digital.supercharger.dto.UserCredentials;
import org.eclipse.jetty.http.HttpHeader;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("IntegrationTest")
@TestPropertySource("classpath:application.properties")
class AuthIntegrationTest {

  private final TestRestTemplate restTemplate = new TestRestTemplate();
  private final HttpHeaders headers = new HttpHeaders();

  @Value("${local.server.port}")
  private int port;

  @Test
  void loginWithoutCredentials() {
    var userCredentials = new UserCredentials();
    headers.add(HttpHeader.CONTENT_TYPE.asString(), "application/json");
    HttpEntity<UserCredentials> entity = new HttpEntity<>(userCredentials, headers);
    ResponseEntity<ApiError> response =
        restTemplate.exchange(
            createURLWithPort(TestHelper.AUTH_URL + "/login"),
            HttpMethod.POST,
            entity,
            ApiError.class);

    ApiError apiError = response.getBody();

    assertNotNull(apiError);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertEquals("Validation error", apiError.getMessage());
    assertEquals(2, apiError.getErrors().size());
  }

  private String createURLWithPort(String uri) {
    return "http://localhost:" + port + uri;
  }
}
