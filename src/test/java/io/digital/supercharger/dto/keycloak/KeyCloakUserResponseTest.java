package io.digital.supercharger.dto.keycloak;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.jupiter.api.Test;

class KeyCloakUserResponseTest {

  @Test
  void testKeyCloakUserResponse() {

    DecodedJWT jwt =
        JWT.decode(
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIxRUR5bVBJMXItNUxYbXF6RDhwV1EzdlpSZTdIMmRQWGdBZVc1dElGeW5VIn0.eyJleHAiOjE1ODg3NzcwNTYsImlhdCI6MTU4ODc3Njc1NiwianRpIjoiMTgzMzNiNDYtMWRhZS00Yzc4LTk0YmItYTUxYWIxZDhlODFhIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MTgwL2F1dGgvcmVhbG1zL2RldiIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlZDc2YTEyMi05MTAyLTQ1MzEtODJjNC0xYjcxZjAzYjM3NjkiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjbGllbnQtY29uZmlkZW50aWFsLWFwcCIsInNlc3Npb25fc3RhdGUiOiIyYzI0NTc0Ni1lNzE3LTQzYjgtOTdhYy1kMTMzYzRiMzg5ZTYiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJjaWYiOjEyMzQ1Njc4OSwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJHYWJyaWVsIEFqYWJhaGlhbiIsInBob25lX251bWJlciI6IiszMTYyODI2MTAwNyIsInByZWZlcnJlZF91c2VybmFtZSI6ImRlbW8iLCJnaXZlbl9uYW1lIjoiR2FicmllbCIsImZhbWlseV9uYW1lIjoiQWphYmFoaWFuIiwicGljdHVyZSI6Imh0dHBzOi8vbWVkaWEtZXhwMS5saWNkbi5jb20vZG1zL2ltYWdlL0M1NjAzQVFFblVib3M2OVRXTVEvcHJvZmlsZS1kaXNwbGF5cGhvdG8tc2hyaW5rXzIwMF8yMDAvMD9lPTE1OTQyNTI4MDAmdj1iZXRhJnQ9Tng2MkxBeE1hNURFakFJdzVVeThINjMtRWVQZHBIMHloYUh2Q2tQcmpwRSIsImVtYWlsIjoiZ2FqYWJhaGlhbkB0ZXN0LmNvbSJ9.fn5oiGS1AQM9iNQ-7LN0h2a9xIUgLM1QJrwE8sbeAWmmOd6GGVYY5Dme3QYjntFPm42VKyD7KQkjxJn56ip7YDFFs4lCvYXE4ay003ABD8YC5M0wbfhggW6QXMEqqCvBtJKuKE4dDmUq9iJ_dmCcEz6wgMSkgz5uvkfguqOKUYH4gumGfuVSPjGJXTFKeMxSv3ecL9aVNcqpdWuHk6OBczpBWOMCgXyGs0g6MAfozzlLzos2ybi9Gau2rELEBujUbexvb74G7yLoF-6dljGfjsE22iCpjuoE-feOJmAZ-VoYz7PitOzrLl0W8D9k-9vwVuoJy-_ORBccr_FNvwImYg");
    var keyCloakUserResponse = new KeyCloakUserResponse(jwt.getClaims());
    assertEquals(123456789, keyCloakUserResponse.getCif());
    assertEquals("demo", keyCloakUserResponse.getPreferredUsername());
    assertEquals("Gabriel", keyCloakUserResponse.getGivenName());
    assertEquals("Ajabahian", keyCloakUserResponse.getFamilyName());
    assertEquals("+31628261007", keyCloakUserResponse.getPhoneNumber());
    assertEquals(
        "https://media-exp1.licdn.com/dms/image/C5603AQEnUbos69TWMQ/profile-displayphoto-shrink_200_200/0?e=1594252800&v=beta&t=Nx62LAxMa5DEjAIw5Uy8H63-EePdpH0yhaHvCkPrjpE",
        keyCloakUserResponse.getPicture());
    assertEquals("ed76a122-9102-4531-82c4-1b71f03b3769", keyCloakUserResponse.getSub());
    assertEquals("gajabahian@test.com", keyCloakUserResponse.getEmail());
    assertEquals(false, keyCloakUserResponse.isActive());
    assertEquals(true, keyCloakUserResponse.isEmailVerified());
  }
}
