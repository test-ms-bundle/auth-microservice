package io.digital.supercharger.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.config.KeyCloakConfig;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.keycloak.KeyCloakAuthResponse;
import io.digital.supercharger.dto.keycloak.KeyCloakUserResponse;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(SpringExtension.class)
class KeyCloakUserServiceTest {
  private final String serverUrl = "http://testkeycloak";
  private final String realm = "dev";
  private final String clientId = "test";
  private final String clientSecret = "tes1t";
  @Mock private RestTemplate restTemplate;
  @Mock private KeyCloakConfig keyCloakConfig;
  private KeyCloakUserService keyCloakUserService;

  @BeforeEach
  void initEveryTest() {
    when(keyCloakConfig.getServerUrl()).thenReturn(serverUrl);
    when(keyCloakConfig.getRealm()).thenReturn(realm);
    when(keyCloakConfig.getClientId()).thenReturn(clientId);
    when(keyCloakConfig.getClientSecret()).thenReturn(clientSecret);
    keyCloakUserService = new KeyCloakUserService(keyCloakConfig, restTemplate);
    verify(keyCloakConfig, times(1)).getServerUrl();
  }

  @Test
  void login() {
    final String username = "username";
    final String password = "password";
    var ipAddress = "127.0.0.1";
    var userCredentials = new UserCredentials(username, password);

    var url = getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/token");

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    when(restTemplate.postForEntity(eq(url), any(), eq(KeyCloakAuthResponse.class)))
        .thenReturn(new ResponseEntity<>(keyCloakAuthResponse, HttpStatus.OK));

    var logInUser = keyCloakUserService.login(userCredentials, ipAddress);
    verify(restTemplate, times(1)).postForEntity(eq(url), any(), eq(KeyCloakAuthResponse.class));

    assertEquals("token1", logInUser.getAccessToken());
    assertEquals("token2", logInUser.getRefreshToken());
    assertEquals(1, logInUser.getExpiresIn());
    assertEquals(2, logInUser.getRefreshExpiresIn());
    assertEquals("bearer", logInUser.getTokenType());
  }

  @Test
  void refreshToken() {
    final String oldToken = "oldToken";
    final String newToken = "newToken";
    var ipAddress = "127.0.0.1";

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    var url = getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/token");
    when(restTemplate.postForEntity(eq(url), any(), eq(KeyCloakAuthResponse.class)))
        .thenReturn(new ResponseEntity<>(keyCloakAuthResponse, HttpStatus.OK));

    var logInUser = keyCloakUserService.refreshToken(oldToken, newToken, ipAddress);
    verify(restTemplate, times(1)).postForEntity(eq(url), any(), eq(KeyCloakAuthResponse.class));

    assertEquals("token1", logInUser.getAccessToken());
    assertEquals("token2", logInUser.getRefreshToken());
    assertEquals(1, logInUser.getExpiresIn());
    assertEquals(2, logInUser.getRefreshExpiresIn());
    assertEquals("bearer", logInUser.getTokenType());
  }

  @Test
  void logout() {
    final String oldToken = "oldToken";
    final String newToken = "newToken";

    var url = getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/logout");
    when(restTemplate.postForEntity(eq(url), any(), eq(ResponseEntity.class)))
        .thenReturn(new ResponseEntity<>(null, HttpStatus.NO_CONTENT));

    var logInUser = keyCloakUserService.logout(oldToken, newToken);
    verify(restTemplate, times(1)).postForEntity(eq(url), any(), eq(ResponseEntity.class));

    assertEquals(HttpStatus.NO_CONTENT, logInUser.getStatusCode());
  }

  @Test
  void updatePassword() throws JsonProcessingException {
    final String userId = "1";
    final String password = "newPass";
    final String ipAddress = "1234567";

    adminLoginLogout();

    var url =
        keyCloakConfig.getServerUrl()
            + "/auth/admin/realms/"
            + keyCloakConfig.getRealm()
            + "/users/"
            + userId
            + "/reset-password";

    when(restTemplate.exchange(eq(url), eq(HttpMethod.PUT), any(), eq(ResponseEntity.class)))
        .thenReturn(new ResponseEntity<>(null, HttpStatus.NO_CONTENT));

    var changePass = keyCloakUserService.updatePassword(userId, password, ipAddress);
    verify(restTemplate, times(1))
        .exchange(eq(url), eq(HttpMethod.PUT), any(), eq(ResponseEntity.class));

    assertEquals(HttpStatus.NO_CONTENT, changePass.getStatusCode());
  }

  @Test
  void updatePasswordError() throws JsonProcessingException {
    final String userId = "1";
    final String password = "newPass";
    final String ipAddress = "1234567";

    adminLoginLogout();

    var url =
        keyCloakConfig.getServerUrl()
            + "/auth/admin/realms/"
            + keyCloakConfig.getRealm()
            + "/users/"
            + userId
            + "/reset-password";

    when(restTemplate.exchange(eq(url), eq(HttpMethod.PUT), any(), eq(ResponseEntity.class)))
        .thenReturn(new ResponseEntity<>(null, HttpStatus.NO_CONTENT));

    var changePass = keyCloakUserService.updatePassword(userId, password, ipAddress);

    verify(restTemplate, times(1))
        .exchange(eq(url), eq(HttpMethod.PUT), any(), eq(ResponseEntity.class));

    assertEquals(HttpStatus.NO_CONTENT, changePass.getStatusCode());
  }

  @Test
  void createUser() throws JsonProcessingException {
    CreateClientInfo createClientInfo =
        CreateClientInfo.builder()
            .username("test")
            .cif(123456)
            .email("test@test.com")
            .emailVerified(true)
            .firstName("test")
            .lastName("test 1")
            .name("some name")
            .password("Test@@#")
            .groups(List.of("Clients"))
            .phoneNumber("123456")
            .picture("Some Picture Link")
            .build();
    ;
    final String ipAddress = "1234567";

    adminLoginLogout();

    var createUserUrl =
        keyCloakConfig.getServerUrl()
            + "/auth/admin/realms/"
            + keyCloakConfig.getRealm()
            + "/users";

    when(restTemplate.exchange(
            eq(createUserUrl), eq(HttpMethod.POST), any(), eq(ResponseEntity.class)))
        .thenReturn(new ResponseEntity<>(null, HttpStatus.CREATED));

    var createUser = keyCloakUserService.createUser(createClientInfo, ipAddress);
    verify(restTemplate, times(1))
        .exchange(eq(createUserUrl), eq(HttpMethod.POST), any(), eq(ResponseEntity.class));

    assertEquals(HttpStatus.CREATED, createUser.getStatusCode());
  }

  private void adminLoginLogout() {
    var adminLogin = getOpenIdConnectUrl(keyCloakConfig.getAdminRealm(), "/token");

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    when(restTemplate.postForEntity(eq(adminLogin), any(), eq(KeyCloakAuthResponse.class)))
        .thenReturn(new ResponseEntity<>(keyCloakAuthResponse, HttpStatus.OK));

    var adminLogout = getOpenIdConnectUrl(keyCloakConfig.getAdminRealm(), "/logout");
    when(restTemplate.postForEntity(eq(adminLogout), any(), eq(ResponseEntity.class)))
        .thenReturn(new ResponseEntity<>(null, HttpStatus.NO_CONTENT));
  }

  @Test
  void getTokenIntrospect() {
    final String token = "Token";

    final String id = "1111";
    final String email = "test@test.com";
    final String familyName = "test";
    final String givenName = "givenName";
    final String name = "test givenName";
    final String phoneNumber = "123455";
    final String username = "username";
    final String picture = "http://www.google.com/link";
    final long cif = 12345;
    final boolean active = true;
    final boolean emailVerified = true;

    var keyCloakUserResponse =
        KeyCloakUserResponse.builder()
            .active(active)
            .cif(cif)
            .email(email)
            .emailVerified(emailVerified)
            .familyName(familyName)
            .givenName(givenName)
            .name(name)
            .phoneNumber(phoneNumber)
            .preferredUsername(username)
            .picture(picture)
            .sub(id)
            .build();

    var url = getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/token/introspect");
    when(restTemplate.postForEntity(eq(url), any(), eq(KeyCloakUserResponse.class)))
        .thenReturn(new ResponseEntity<>(keyCloakUserResponse, HttpStatus.OK));

    var userInfo = keyCloakUserService.getTokenIntrospect(token);
    verify(restTemplate, times(1)).postForEntity(eq(url), any(), eq(KeyCloakUserResponse.class));

    assertEquals(id, userInfo.getSub());
    assertEquals(email, userInfo.getEmail());
    assertEquals(familyName, userInfo.getFamilyName());
    assertEquals(givenName, userInfo.getGivenName());
    assertEquals(name, userInfo.getName());
    assertEquals(phoneNumber, userInfo.getPhoneNumber());
    assertEquals(username, userInfo.getPreferredUsername());
    assertEquals(picture, userInfo.getPicture());
    assertEquals(cif, userInfo.getCif());
  }

  @Test
  void getUserInfo() {
    final String token = "Token";

    final String id = "1111";
    final String email = "test@test.com";
    final String familyName = "test";
    final String givenName = "givenName";
    final String name = "test givenName";
    final String phoneNumber = "123455";
    final String username = "username";
    final String picture = "http://www.google.com/link";
    final long cif = 12345;
    final boolean active = true;
    final boolean emailVerified = true;

    var keyCloakUserResponse =
        KeyCloakUserResponse.builder()
            .active(active)
            .cif(cif)
            .email(email)
            .emailVerified(emailVerified)
            .familyName(familyName)
            .givenName(givenName)
            .name(name)
            .phoneNumber(phoneNumber)
            .preferredUsername(username)
            .picture(picture)
            .sub(id)
            .build();

    var url = getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/userinfo");
    when(restTemplate.postForEntity(eq(url), any(), eq(KeyCloakUserResponse.class)))
        .thenReturn(new ResponseEntity<>(keyCloakUserResponse, HttpStatus.OK));

    var userInfo = keyCloakUserService.getUserInfo(token);
    verify(restTemplate, times(1)).postForEntity(eq(url), any(), eq(KeyCloakUserResponse.class));

    assertEquals(id, userInfo.getSub());
    assertEquals(email, userInfo.getEmail());
    assertEquals(familyName, userInfo.getFamilyName());
    assertEquals(givenName, userInfo.getGivenName());
    assertEquals(name, userInfo.getName());
    assertEquals(phoneNumber, userInfo.getPhoneNumber());
    assertEquals(username, userInfo.getPreferredUsername());
    assertEquals(picture, userInfo.getPicture());
    assertEquals(cif, userInfo.getCif());
  }

  @Test
  void getOpenIdCert() {
    var url = keyCloakUserService.getOpenIdCert();
    assertEquals(
        keyCloakConfig.getServerUrl() + getOpenIdConnectUrl(keyCloakConfig.getRealm(), "/certs"),
        url);
  }

  private String getOpenIdConnectUrl(String realm, String path) {
    return "/auth/realms/" + realm + "/protocol/openid-connect" + path;
  }
}
