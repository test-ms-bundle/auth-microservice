package io.digital.supercharger.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.digital.supercharger.dto.CreateClientInfo;
import io.digital.supercharger.dto.UserCredentials;
import io.digital.supercharger.dto.UserPassword;
import io.digital.supercharger.dto.keycloak.KeyCloakAuthResponse;
import io.digital.supercharger.dto.keycloak.KeyCloakUserResponse;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;

@ExtendWith(SpringExtension.class)
class DefaultAuthServiceTest {

  @Mock private KeyCloakUserService keyCloakUserService;
  private DefaultAuthService defaultAuthService;

  @BeforeEach
  void initEveryTest() {
    defaultAuthService = new DefaultAuthService(keyCloakUserService);
  }

  @Test
  void login() {
    final String username = "username";
    final String password = "password";
    var ipAddress = "127.0.0.1";
    var userCredentials = new UserCredentials(username, password);

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    when(keyCloakUserService.login(
            argThat(
                login ->
                    login.getUsername().equals(username) && login.getPassword().equals(password)),
            eq(ipAddress)))
        .thenReturn(keyCloakAuthResponse);

    var logInUser = defaultAuthService.login(userCredentials, ipAddress);
    verify(keyCloakUserService, times(1))
        .login(
            argThat(
                login ->
                    login.getUsername().equals(username) && login.getPassword().equals(password)),
            eq(ipAddress));

    assertEquals("token1", logInUser.getAccessToken());
    assertEquals("token2", logInUser.getRefreshToken());
    assertEquals(1, logInUser.getExpiresIn());
    assertEquals(2, logInUser.getRefreshExpiresIn());
    assertEquals("bearer", logInUser.getTokenType());
  }

  @Test
  void refreshToken() {
    final String token = "username";
    final String refreshToken = "password";
    var ipAddress = "127.0.0.1";

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    when(keyCloakUserService.refreshToken(eq(token), eq(refreshToken), eq(ipAddress)))
        .thenReturn(keyCloakAuthResponse);

    var logInUser = defaultAuthService.refreshToken(token, refreshToken, ipAddress);
    verify(keyCloakUserService, times(1)).refreshToken(eq(token), eq(refreshToken), eq(ipAddress));

    assertEquals("token1", logInUser.getAccessToken());
    assertEquals("token2", logInUser.getRefreshToken());
    assertEquals(1, logInUser.getExpiresIn());
    assertEquals(2, logInUser.getRefreshExpiresIn());
    assertEquals("bearer", logInUser.getTokenType());
  }

  @Test
  void getUserInfo() {
    final String token = "username";
    final String id = "1111";
    final String email = "test@test.com";
    final String familyName = "test";
    final String givenName = "givenName";
    final String name = "test givenName";
    final String phoneNumber = "123455";
    final String username = "username";
    final String picture = "http://www.google.com/link";
    final long cif = 12345;
    final boolean active = true;
    final boolean emailVerified = true;

    var keyCloakUserResponse =
        KeyCloakUserResponse.builder()
            .active(active)
            .cif(cif)
            .email(email)
            .emailVerified(emailVerified)
            .familyName(familyName)
            .givenName(givenName)
            .name(name)
            .phoneNumber(phoneNumber)
            .preferredUsername(username)
            .picture(picture)
            .sub(id)
            .build();

    when(keyCloakUserService.getUserInfo(eq(token))).thenReturn(keyCloakUserResponse);

    var userInfo = defaultAuthService.getUserInfo(token, false);
    verify(keyCloakUserService, times(1)).getUserInfo(eq(token));

    assertEquals(id, userInfo.getId());
    assertEquals(email, userInfo.getEmail());
    assertEquals(familyName, userInfo.getLastName());
    assertEquals(givenName, userInfo.getFirstName());
    assertEquals(name, userInfo.getName());
    assertEquals(phoneNumber, userInfo.getPhoneNumber());
    assertEquals(username, userInfo.getUsername());
    assertEquals(picture, userInfo.getPicture());
    assertEquals(cif, userInfo.getCif());
  }

  @Test
  void getUserInfoOfflineError() {
    final String token = "wrongToken";

    assertThrows(
        HttpClientErrorException.class,
        () -> defaultAuthService.getUserInfo(token, true),
        HttpStatus.UNAUTHORIZED.getReasonPhrase());
    verify(keyCloakUserService, times(0)).getOpenIdCert();
    verify(keyCloakUserService, times(0)).getUserInfo(eq(token));
  }

  @Test
  void getUserInfoOfflineFailedVerificationToken() {
    final String token =
        "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIxRUR5bVBJMXItNUxYbXF6RDhwV1EzdlpSZTdIMmRQWGdBZVc1dElGeW5VIn0.eyJleHAiOjE1ODg3NTM3MzgsImlhdCI6MTU4ODc1MzQzOCwianRpIjoiNGE4ZmMyYjYtZWU5MC00YzE0LWI3YmMtZmQxMjFmM2E5ODBlIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MTgwL2F1dGgvcmVhbG1zL2RldiIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlZDc2YTEyMi05MTAyLTQ1MzEtODJjNC0xYjcxZjAzYjM3NjkiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjbGllbnQtY29uZmlkZW50aWFsLWFwcCIsInNlc3Npb25fc3RhdGUiOiI5ZTI2MzQ2Zi01ZGIzLTQ5MDYtYTI3Yy00ZGMzN2VmNTZlYTYiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJjaWYiOjEyMzQ1Njc4OSwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJEZW1vIFVzZXIiLCJwaG9uZV9udW1iZXIiOiIrMzE2MjgyNjEwMDYiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJkZW1vIiwiZ2l2ZW5fbmFtZSI6IkRlbW8iLCJmYW1pbHlfbmFtZSI6IlVzZXIiLCJwaWN0dXJlIjoiaHR0cHM6Ly9tZWRpYS1leHAxLmxpY2RuLmNvbS9kbXMvaW1hZ2UvQzU2MDNBUUVuVWJvczY5VFdNUS9wcm9maWxlLWRpc3BsYXlwaG90by1zaHJpbmtfMjAwXzIwMC8wP2U9MTU5NDI1MjgwMCZ2PWJldGEmdD1OeDYyTEF4TWE1REVqQUl3NVV5OEg2My1FZVBkcEgweWhhSHZDa1ByanBFIiwiZW1haWwiOiJkZW1vQHRlc3QuY29tIn0.iKAQNYjH9dLqSCqWCHTX_7PAHms7tU8Pegjtj4PetC4UPmDIMhVkqJspNVRpFKzGMdmsjYKN4lohSHX99te2RL7nhNQkIjUTr-rlZKcY_Uj1LZlVrWAvMNIq33OjFotZsO9Yzfwr6w16aMxJhU3TsQMIfoKYoxSR22MKlH4idQ7ukqifD-P8EvG9GuAhNf5ie89C2gzlNqEDweaUXBsXY_j_AlkN2SX1wuwpsEXBJX2crM60e8Jg19BCrcpt5LH34NJ-ORQwb57jDN6IVIF9ObY4gwXva8qcbmOqbOZiWT72A6QMttwp8s6NX3wOh4qcICARQYMGJfx1qLNfmofTDQ";

    when(keyCloakUserService.getOpenIdCert()).thenReturn("http://localhost/cert");
    assertThrows(
        HttpClientErrorException.class,
        () -> defaultAuthService.getUserInfo(token, true),
        HttpStatus.UNAUTHORIZED.getReasonPhrase());
    verify(keyCloakUserService, times(1)).getOpenIdCert();
    verify(keyCloakUserService, times(0)).getUserInfo(eq(token));
  }

  @Test
  void getUserInfoOfflineMailFormedToken() {
    final String token =
        "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIxRUR5bVBJMXItNUxYbXF6RDhwV1EzdlpSZTdIMmRQWGdBZVc1dElGeW5VIn0.eyJleHAiOjE1ODg3NTM3MzgsImlhdCI6MTU4ODc1MzQzOCwianRpIjoiNGE4ZmMyYjYtZWU5MC00YzE0LWI3YmMtZmQxMjFmM2E5ODBlIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MTgwL2F1dGgvcmVhbG1zL2RldiIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlZDc2YTEyMi05MTAyLTQ1MzEtODJjNC0xYjcxZjAzYjM3NjkiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjbGllbnQtY29uZmlkZW50aWFsLWFwcCIsInNlc3Npb25fc3RhdGUiOiI5ZTI2MzQ2Zi01ZGIzLTQ5MDYtYTI3Yy00ZGMzN2VmNTZlYTYiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJjaWYiOjEyMzQ1Njc4OSwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJEZW1vIFVzZXIiLCJwaG9uZV9udW1iZXIiOiIrMzE2MjgyNjEwMDYiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJkZW1vIiwiZ2l2ZW5fbmFtZSI6IkRlbW8iLCJmYW1pbHlfbmFtZSI6IlVzZXIiLCJwaWN0dXJlIjoiaHR0cHM6Ly9tZWRpYS1leHAxLmxpY2RuLmNvbS9kbXMvaW1hZ2UvQzU2MDNBUUVuVWJvczY5VFdNUS9wcm9maWxlLWRpc3BsYXlwaG90by1zaHJpbmtfMjAwXzIwMC8wP2U9MTU5NDI1MjgwMCZ2PWJldGEmdD1OeDYyTEF4TWE1REVqQUl3NVV5OEg2My1FZVBkcEgweWhhSHZDa1ByanBFIiwiZW1haWwiOiJkZW1vQHRlc3QuY29tIn0.iKAQNYjH9dLqSCqWCHTX_7PAHms7tU8Pegjtj4PetC4UPmDIMhVkqJspNVRpFKzGMdmsjYKN4lohSHX99te2RL7nhNQkIjUTr-rlZKcY_Uj1LZlVrWAvMNIq33OjFotZsO9Yzfwr6w16aMxJhU3TsQMIfoKYoxSR22MKlH4idQ7ukqifD-P8EvG9GuAhNf5ie89C2gzlNqEDweaUXBsXY_j_AlkN2SX1wuwpsEXBJX2crM60e8Jg19BCrcpt5LH34NJ-ORQwb57jDN6IVIF9ObY4gwXva8qcbmOqbOZiWT72A6QMttwp8s6NX3wOh4qcICARQYMGJfx1qLNfmofTDQ";

    when(keyCloakUserService.getOpenIdCert()).thenReturn("");
    assertThrows(
        HttpClientErrorException.class,
        () -> defaultAuthService.getUserInfo(token, true),
        HttpStatus.UNAUTHORIZED.getReasonPhrase());
    verify(keyCloakUserService, times(1)).getOpenIdCert();
    verify(keyCloakUserService, times(0)).getUserInfo(eq(token));
  }

  @Test
  void getTokenIntrospect() {
    final String token = "username";
    final String id = "1111";
    final String email = "test@test.com";
    final String familyName = "test";
    final String givenName = "givenName";
    final String name = "test givenName";
    final String phoneNumber = "123455";
    final String username = "username";
    final String picture = "http://www.google.com/link";
    final long cif = 12345;
    final boolean active = true;
    final boolean emailVerified = true;

    var keyCloakUserResponse =
        KeyCloakUserResponse.builder()
            .active(active)
            .cif(cif)
            .email(email)
            .emailVerified(emailVerified)
            .familyName(familyName)
            .givenName(givenName)
            .name(name)
            .phoneNumber(phoneNumber)
            .preferredUsername(username)
            .picture(picture)
            .sub(id)
            .build();

    when(keyCloakUserService.getTokenIntrospect(eq(token))).thenReturn(keyCloakUserResponse);

    var userInfo = defaultAuthService.getTokenIntrospect(token);
    verify(keyCloakUserService, times(1)).getTokenIntrospect(eq(token));

    assertEquals(id, userInfo.getId());
    assertEquals(email, userInfo.getEmail());
    assertEquals(familyName, userInfo.getLastName());
    assertEquals(givenName, userInfo.getFirstName());
    assertEquals(name, userInfo.getName());
    assertEquals(phoneNumber, userInfo.getPhoneNumber());
    assertEquals(username, userInfo.getUsername());
    assertEquals(picture, userInfo.getPicture());
    assertEquals(cif, userInfo.getCif());
  }

  @Test
  void getTokenIntrospectNotActive() {
    final String token = "username";
    final String id = "1111";
    final String email = "test@test.com";
    final String familyName = "test";
    final String givenName = "givenName";
    final String name = "test givenName";
    final String phoneNumber = "123455";
    final String username = "username";
    final String picture = "http://www.google.com/link";
    final long cif = 12345;
    final boolean active = false;
    final boolean emailVerified = true;

    var keyCloakUserResponse =
        KeyCloakUserResponse.builder()
            .active(active)
            .cif(cif)
            .email(email)
            .emailVerified(emailVerified)
            .familyName(familyName)
            .givenName(givenName)
            .name(name)
            .phoneNumber(phoneNumber)
            .preferredUsername(username)
            .picture(picture)
            .sub(id)
            .build();

    when(keyCloakUserService.getTokenIntrospect(eq(token))).thenReturn(keyCloakUserResponse);
    assertThrows(
        HttpClientErrorException.class,
        () -> defaultAuthService.getTokenIntrospect(token),
        HttpStatus.UNAUTHORIZED.getReasonPhrase());
    verify(keyCloakUserService, times(1)).getTokenIntrospect(eq(token));
  }

  @Test
  void logout() {
    final String token = "token";
    final String refreshToken = "refreshToken";

    when(keyCloakUserService.logout(eq(token), eq(refreshToken)))
        .thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    var logInUser = defaultAuthService.logout(token, refreshToken);
    verify(keyCloakUserService, times(1)).logout(eq(token), eq(refreshToken));

    assertEquals(HttpStatus.NO_CONTENT, logInUser.getStatusCode());
  }

  @Test
  void updatePassword() throws JsonProcessingException {
    final String token = "token";
    final String userId = "1234";
    final String testUsername = "testUsername";
    final String currentPassword = "pass1";
    final String newPassword = "pass2";
    final String ipAddress = "127.0.0.1";
    UserPassword updatePassword = new UserPassword(currentPassword, newPassword);

    when(keyCloakUserService.getTokenIntrospect(eq(token)))
        .thenReturn(
            KeyCloakUserResponse.builder()
                .sub(userId)
                .active(true)
                .preferredUsername(testUsername)
                .build());

    var keyCloakAuthResponse = new KeyCloakAuthResponse();
    keyCloakAuthResponse.setAccessToken("token1");
    keyCloakAuthResponse.setExpiresIn(1);
    keyCloakAuthResponse.setRefreshExpiresIn(2);
    keyCloakAuthResponse.setRefreshToken("token2");
    keyCloakAuthResponse.setTokenType("bearer");

    when(keyCloakUserService.login(
            argThat(
                login ->
                    login.getUsername().equals(testUsername)
                        && login.getPassword().equals(currentPassword)),
            eq(ipAddress)))
        .thenReturn(keyCloakAuthResponse);

    when(keyCloakUserService.updatePassword(eq(userId), eq(newPassword), eq(ipAddress)))
        .thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));

    var updatePassword1 = defaultAuthService.updatePassword(token, updatePassword, ipAddress);
    assertEquals(HttpStatus.NO_CONTENT, updatePassword1.getStatusCode());

    verify(keyCloakUserService, times(1))
        .updatePassword(eq(userId), eq(newPassword), eq(ipAddress));
    verify(keyCloakUserService, times(1)).getTokenIntrospect(eq(token));
    verify(keyCloakUserService, times(1))
        .login(
            argThat(
                login ->
                    login.getUsername().equals(testUsername)
                        && login.getPassword().equals(currentPassword)),
            eq(ipAddress));
  }

  @Test
  void createUser() throws JsonProcessingException {
    var ipAddress = "127.0.0.1";
    CreateClientInfo createClientInfo =
        CreateClientInfo.builder()
            .username("test")
            .cif(123456)
            .email("test@test.com")
            .emailVerified(true)
            .firstName("test")
            .lastName("test 1")
            .name("some name")
            .password("Test@@#")
            .groups(List.of("Clients"))
            .phoneNumber("123456")
            .picture("Some Picture Link")
            .build();

    when(keyCloakUserService.createUser(
            argThat(
                createClientInfoArg ->
                    createClientInfoArg.getEmail().equals(createClientInfo.getEmail())
                        && createClientInfoArg
                            .getPassword()
                            .equals(createClientInfo.getPassword())),
            eq(ipAddress)))
        .thenReturn(new ResponseEntity<>(HttpStatus.CREATED));

    var createdUser = defaultAuthService.create(createClientInfo, ipAddress);
    verify(keyCloakUserService, times(1))
        .createUser(
            argThat(
                createClientInfoArg ->
                    createClientInfoArg.getEmail().equals(createClientInfo.getEmail())
                        && createClientInfoArg
                            .getPassword()
                            .equals(createClientInfo.getPassword())),
            eq(ipAddress));

    assertEquals(HttpStatus.CREATED, createdUser.getStatusCode());
  }
}
