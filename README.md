# Auth microservice java

Spring Authentication application is a Java SpringBoot microservice that exposes some REST APIs. The microservice follow the domain-driven design pattern e.g. it has controllers, model, services and repositories. The microservice is part of a small group of microservices used as a playground to provide a boilerplate for:

- Backend persistence in KeyCloak
- Monitoring and health checks [Health Url](http://localhost:8080/actuator)

# Requirements:

- Apache Maven 3.3
- Java 11

# Application Architecture & Technology Stack

This repository implements the following quality gates:

- Static code checks: by running [Checkstyle](http://checkstyle.sourceforge.net/), [PMD](https://pmd.github.io/) and [SpotBugs](https://spotbugs.github.io/) to check the code for any style or quality issues. Checkstyle is based on the [Google Java Standards](https://google.github.io/styleguide/javaguide.html) 
- Static code analysis using [SonarQube](https://www.sonarqube.org/)
- Unit testing: using [JUnit](https://junit.org/junit4/), mocking using [JMockit](http://jmockit.github.io/) and [Mockito](https://site.mockito.org/) (using both only as an example of each)
- Integration testing: using [SpringBootTest](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html). A good example can be found [here](https://dzone.com/articles/integration-testing-in-spring-boot-1)
- Code coverage: generating code coverage reports using the [JaCoCo](https://www.jacoco.org/jacoco/)
- Docker image build: using the Spotify [docker maven plugin](https://github.com/spotify/docker-maven-plugin)
- Pushing to image registry: pushing the docker image to the [Docker hub](https://hub.docker.com/)

These steps can be run manually or using a Continuous Integration tool such as [Jenkins](https://jenkins.io/). More information about setting up Jenkins and SonarQube are explained in the [Build via Jenkins (Jenkinsfile)](build-via-jenkins-jenkinsfile) section below

## Major Libraries / Tools

| Category                           | Library/Tool      | Link                                                          |
|---------------------------------   |----------------   |------------------------------------------------------------   |
| Microservice Framework             | Spring Boot       | https://spring.io/projects/spring-boot                        |
| Dependencies Management            | Spring            | https://docs.spring.io                                        |
| Object Relational Mapping (ORM)    | Spring Data JPA   | https://spring.io/projects/spring-data-jpa                    |
| Boilerplate Code Generation        | Lombok (see below) | https://projectlombok.org/                                   |
| Automate Build & Release           | Maven             | https://docs.fastlane.tools/actions/xcodebuild/               |
| Unit Testing                       | JUnit 5           | https://junit.org/junit5/                                     |
| Mocking (examples of two libraries | Mockito & JMockit | http://jmockit.github.io/ and https://site.mockito.org/       |
| Code Coverage                      | JaCoCo            | https://www.jacoco.org/jacoco/                                |
| Static Code Style Check (Lint)     | Checkstyle        | http://checkstyle.sourceforge.net/                            |
| Static Code Analysis               | PMD and SpotBugs  | https://pmd.github.io/ and https://spotbugs.github.io/        |
| Integration Testing                | SpringBootTest    | https://dzone.com/articles/integration-testing-in-spring-boot-1    |
| Docker Image Storage               | Docker Hub        | https://hub.docker.com/                                       |
| Continous Integration              | Jenkins           | https://jenkins.io/                                           |
| Static Code Analysis Integration   | SonarQube         | https://www.sonarqube.org/                                    |

## Below is a list of Maven plugins used:

* **spring-boot-maven-plugin**
* **maven-jar-plugin**
* **maven-dependency-plugin**
* **git-commit-id-plugin**: extract part of the Git commit SHA
* **dockerfile-maven-plugin**: package the jar as a Docker container
* **maven-checkstyle-plugin**: run Checkstyle using the [./checkstyle.xml](./checkstyle.xml) in the root of the project. This is based on the [Google Java Style](https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml)
* **spotbugs-maven-plugin**: run Spotbugs static analysis
* **maven-pmd-plugin**: run PMD static analysis
* **maven-surefire-plugin**: run the unit tests
* **maven-failsafe-plugin**: run the integration tests
* **jacoco-maven-plugin**: run the code coverage
* **awaitility**: to handle the thread wait during testing async methods

## Boilerplate Code Generation Using Lombok

This project used [Lombok](https://projectlombok.org/) to automatically generate boilerplate code such as getter, setters, constructors, etc. It even includes a builder patter. Lombok is compile time only and is setup in Maven as `provided`. To install it for Eclipse follow the instructions [here](https://projectlombok.org/setup/eclipse).

### Before running auth you need common installed
```
git clone https://gabriel-a@bitbucket.org/test-ms-bundle/supercharger-common-lib.git
mvn clean install
```

### How to start application with ElasticApm running on domain http://elastic-apm.local.host without secret

To run with APM server.
```
./run-with-apm.sh
```

Which will run this command.
```
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-javaagent:-javaagent:apm-lib/elastic-apm-agent.jar -Delastic.apm.service_name=auth-microservice -Delastic.apm.application_packages=io.digital.supercharger -Delastic.apm.server_urls=http://elastic-apm.local.host"
```

# Getting Started

Clone and build the project

```bash
    mvn clean install
```

# Running project

This application relies on KeyCloak backend service.

To test locally:
## Start KeyCloak
```
docker run -d --name keycloak -p 8180:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e PROXY_ADDRESS_FORWARDING=true jboss/keycloak
```
Running with "PROXY_ADDRESS_FORWARDING" is important to allow the application to set the forwarded ip address.

Follow the KeyCloak Postman setup:
[Postman KeyCloak](keycloak-postman/README.MD)

# After running Postman:
http://localhost:8080/swagger-ui.html
- default user to login demo/password

For admin interface:
http://localhost:8180/
admin/admin


## Swagger Docs

**The Swagger Docs can be accessed on the following URLs:**

[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

[http://localhost:8080/v2/api-docs](http://localhost:8080/v2/api-docs)

# CI-CD - Build via Jenkins (Jenkinsfile)

This repo contains a [Jenkinsfile](./Jenkinsfile) [https://jenkins.io/doc/book/pipeline/jenkinsfile/](https://jenkins.io/doc/book/pipeline/jenkinsfile/), which is used to define a Jenkins **declarative pipeline** for CI-CD to build the code, run the quality gates, code coverage, static analysis and deploy to Fabric. Here is an example structure of the Jenkinsfile declarative pipeline:

```
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
```
