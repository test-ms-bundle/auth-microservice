# To Test auth microservice

- First checkout the Auth microservice
- Run the Auth microservice
```
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-javaagent:-javaagent:apm-lib/elastic-apm-agent.jar -Delastic.apm.service_name=auth-microservice -Delastic.apm.application_packages=io.digital.supercharger -Delastic.apm.server_urls=http://elastic-apm.local.host"
```
- Run tests with newman
```
newman run Auth-microservice.postman_collection.json -e Auth-microservice.postman_environment.json
```