# Running the application locally for test

### Simple configuration, that allows Keycloak to handle authentication.
- Import [Postman configuration](Keycloak.postman_collection.json) & [Postman configuration](Keycloak.postman_environment.json)
- Run Admin Login

### To Configure a dev Realm with Client and user please run the following
- Run under Admin > Configure Admin
    - Add Realm
        - The core concept in Keycloak is a Realm. A realm secures and manages security metadata for a set of users, applications, and registered oauth clients. Users can be created within a specific realm within the Administration console.
        - We are using dev Realm to test the Micro Services.
        - This postman call will create Realm Dev 
        with the following options: 
        Login with email (true), 
        require SSL = none (the reason for None is keycloak will be running behind Java Microservice, and the Microservice has to set the "X-Forwarded-For" IP Address to keycloak to capture the user Real IP)
    - Admin Add Client to Realm
        - Clients are entities that can request Keycloak to authenticate a user. client client-confidential-app is the backend services that uses Keycloak to secure and provide a single sign-on solution.
        - The Access Type is "confidential" its set by clientAuthenticatorType. This means each application that needs to communicate with the app needs to send "client_secret" 
        - To allow a user to authenticate from auth microservice, we need to set: "username, password, grant_type, client_id, client_secret" The client secret can be found 
        in KeyCloak or retrieved by the next 2 calls. (Get Clients & Set Client Id, Get Clients By Id Set secret we set the configuration variables with postman)
    - Get Clients & Set Client Id
        - Retrieves the clients, and sets client_confidential_id in postman to be able to retrieve the secret. 
    - Get Clients By Id Set secret
        - Retrieves the new client secret. (This secret is needed in any application to communicate with KeyCloak)
    - Create Group 
        - Create Group Clients (This is an example how to group users in KeyCloak under Clients)
    - Create User
        - This will create user demo with (demo/password) credentials.
