#!/bin/sh
set -e
BASEDIR=$(dirname "$0")

newman run $BASEDIR/Keycloak.postman_collection.json -e $BASEDIR/Keycloak.postman_environment.json
