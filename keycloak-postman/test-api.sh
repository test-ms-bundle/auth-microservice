#!/bin/sh
set -e
BASEDIR=$(dirname "$0")

newman run $BASEDIR/Auth-microservice.postman_collection.json -e $BASEDIR/Auth-microservice.postman_environment.json
