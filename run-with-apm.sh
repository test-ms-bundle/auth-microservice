#!/bin/bash
set -e

#This is an example how to run the application with Elastic APM
ELASTIC_APM=http://elastic-apm.local.host
APP_NAME=auth-microservice
APP_PACKAGE=io.digital.supercharger

mvn spring-boot:run -Dspring-boot.run.jvmArguments="-javaagent:apm-lib/elastic-apm-agent.jar -Delastic.apm.service_name=$APP_NAME -Delastic.apm.application_packages=$APP_PACKAGE -Delastic.apm.server_urls=$ELASTIC_APM"
